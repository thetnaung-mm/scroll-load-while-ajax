CREATE TABLE IF NOT EXISTS `paginate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;


INSERT INTO `test` (`id`, `name`, `description`) VALUES
(1, 'php5.4.23', 'The PHP development team announces the immediate availability of PHP 5.4.23. About 10 bugs were fixed, including a security issue in OpenSSL module (CVE-2013-6420). All PHP 5.4 users are encouraged to upgrade to this version.'),
(2, 'php 5.3.28', 'The PHP development team announces the immediate availability of PHP 5.3.28. This release fixes two security issues in OpenSSL module in PHP 5.3 - CVE-2013-4073 and CVE-2013-6420. All PHP 5.3 users are encouraged to upgrade to PHP 5.3.28 or latest versions of PHP 5.4 or PHP 5.5.'),
(3, 'PHP 5.5.7', 'The PHP development team announces the immediate availability of PHP 5.5.7. This release fixes some bugs against PHP 5.5.6 and it also includes a fix for CVE-2013-6420 in OpenSSL extension. All users are strongly encouraged to upgrade.'),

(4, 'php 5.4.22', 'The PHP development team announces the immediate availability of PHP 5.4.22. About 10 bugs were fixed. All PHP 5.4 users are encouraged to upgrade to this version.'),
(5, 'php 5.5.6', 'The PHP development team announces the immediate availability of PHP 5.5.6. This release fixes some bugs against PHP 5.5.5, and adds some performance improvements.'),
(6, 'quick update', 'On 24 Oct 2013 06:15:39 +0000 Google started saying www.php.net was hosting malware. The Google Webmaster Tools were initially quite delayed in showing the reason why and when they did it looked a lot like a false positive because we had some minified/obfuscated javascript being dynamically injected into userprefs.js. This looked suspicious to us as well, but it was actually written to do exactly that so we were quite certain it was a false positive, but we kept digging.'),
(7, 'php 5.4.21', 'The PHP development team announces the immediate availability of PHP 5.4.21. About 10 bugs were fixed. All PHP 5.4 users are encouraged to upgrade to this version.'),
(8, 'php 5.5.0', 'The PHP development team announces the immediate availability of PHP 5.5.0alpha2. This release adds new features and fix some bugs from alpha1. All users of PHP are encouraged to test this version carefully, and report any bugs in the bug tracking system.');
