<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Auto Loading Records</title>
<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>

<?php
include("config.php");
$results = $mysqli->query("SELECT COUNT(*) as tot FROM paginate");
$total_records = @$results->fetch_object();
$total_rec = ceil($total_records->tot/$limit);
$results->close(); 
?>

<script type="text/javascript">
$(document).ready(function() {
	var initRec = 0; //total loaded record
	var loading  = false; //to prevents multipal ajax loads
	var total_rec = <?php echo $total_rec; ?>; //total records
	
	$('#results').load("autoload.php", {'tot_limit':initRec}, function() {initRec++;}); //load first group
	
	$(window).scroll(function() { //detect page scroll
		
		if($(window).scrollTop() + $(window).height() == $(document).height())  //user scrolled to bottom of the page?
		{
			
			if(initRec <= total_rec && loading==false) //there's more data to load
			{
				loading = true;
				$('.result_div').show(); //show loading image
				
				//load data from the server
				$.post('autoload.php',{'tot_limit': initRec}, function(data){
									
					$("#results").append(data); //append received data

					//hide loading image
					$('.result_div').hide(); //hide loading image
					
					initRec++; 
					loading = false; 
				
				}).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
					
					alert(thrownError); //alert with HTTP error
					$('.result_div').hide(); //hide loading image
					loading = false;
				
				});
				
			}
		}
	});
});
</script>
<style>
body,td,th {font-family: Arial, Times New Roman, Times, serif;font-size: 15px;}
.result_div {background: #F9FFFF;border: 1px solid #E1FFFF;padding: 10px;width: 500px;margin-right: auto;margin-left: auto;}
#results{width: 500px;margin-right: auto;margin-left: auto;}
#resultst ol{margin: 0px;padding: 0px;}
#results li{margin-top: 20px;border-top: 1px dotted #E1FFFF;padding-top: 20px;}
</style>
</head>

<body>

<ol id="results">
</ol>
<div class="result_div" style="display:none" align="center"><img src="ajax-loader.gif"></div>


</body>
</html>
